<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('/bibliotecas')->group(function(){
    Route::get('/', 'BibliotecaController@index');
    Route::get('/{id_biblioteca}', 'BibliotecaController@show');
    
    Route::post('/', 'BibliotecaController@store');
    Route::put('/', 'BibliotecaController@update');
    Route::delete('/', 'BibliotecaController@destroy');
}); 

Route::prefix('/livros')->group(function(){
    Route::get('/', 'LivroController@index');
    Route::get('/{id_livro}', 'LivroController@show');
    
    Route::post('/', 'LivroController@store');
    Route::put('/', 'LivroController@update');
    Route::delete('/', 'LivroController@destroy');
}); 
