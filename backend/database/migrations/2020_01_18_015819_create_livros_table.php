<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLivrosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('livros', function (Blueprint $table) {
            $table->bigIncrements('id_livro');
            $table->string('nome_livro', 50);
            $table->string('autor', 50);
            $table->dateTime('dt_publicacao');
            $table->string('editora', 50);
            $table->integer('quantidade');
            $table->unsignedBigInteger('id_biblioteca');

            $table->foreign('id_biblioteca')->references('id_biblioteca')->on('bibliotecas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('livros');
    }
}
