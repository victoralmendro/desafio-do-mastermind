<?php

namespace App\Http\Controllers;

use Exception;
use App\Livro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class LivroController extends Controller
{
    private $messages = [
        'nome_livro.required' => 'O campo "nome do livro" é necessário para prosseguir.',
        'nome_livro.max' => 'O nome do livro deve ter no máximo 50 caracteres.',
        'editora.max' => 'O nome da editora deve ter no máximo 50 caracteres.',
        'autor.required' => 'O campo "autor" é necessário para prosseguir.',
        'autor.max' => 'O nome da editora deve ter no máximo 50 caracteres.',
        'quantidade.integer' => 'Este campo aceita apenas números inteiros',
        'dt_publicacao.date' => 'A campo "data de publicação" deve ter uma data válida',
        'id_biblioteca.required' => 'O livro só pode ser cadastrado caso esteja sendo vinculado à uma biblioteca'
    ];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, string $action = '')
    {
        switch($action){
            case 'default':
                return Validator::make($data, [
                    'nome_livro' => 'required|string|max:50',
                    'editora' => 'max:50',
                    'autor' => 'required|string|max:50',
                    'quantidade' => 'integer',
                    'dt_publicacao' => 'date',
                    'id_biblioteca' => 'required'
                ], $this->messages);
            break;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $livros = Livro::all();

        return response()->json($livros, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $data = $request->all();
            $validation = $this->validator($data, 'default');
            $errors = $validation->errors();
            
            if(sizeof($errors) > 0)
                return response()->json(['data'=> ['error' => $errors]], 422);
            
            DB::beginTransaction();
            $livro = Livro::create($data);    
            DB::commit();
            
            return response()->json(['data'=> $livro], 200);
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();

            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Livro  $livro
     * @return \Illuminate\Http\Response
     */
    public function show($id_livro)
    {
        $livro = Livro::find($id_livro);
        $status = 200;

        if($livro == null){
            $status = 404;
        }

        return response()->json(
            ['data'=> $livro], 
            $status
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Livro  $livro
     * @return \Illuminate\Http\Response
     */
    public function edit(Livro $livro)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Livro  $livro
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $data = $request->all();
            $validation = $this->validator($data, 'default');
            $errors = $validation->errors();
            
            if(sizeof($errors) > 0)
                return response()->json(['data'=> $errors], 422);
            
            $livro = Livro::find($data['id_livro']);    

            DB::beginTransaction();
            $livro->update($data);   
            DB::commit();
            
            return response()->json(['data'=> $livro], 200);
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();
            
            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Livro  $livro
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $data = $request->all();
            
            DB::beginTransaction();
            $livro = Livro::find($request['id_livro']);
            $livro->forceDelete();
            DB::commit();
            
            return response()->json(
                [
                    'data'=> []
                ], 
                200
            );
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();

            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }
}
