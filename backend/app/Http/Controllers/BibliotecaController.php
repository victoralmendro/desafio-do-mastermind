<?php

namespace App\Http\Controllers;

use Exception;
use App\Biblioteca;
use App\Livro;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class BibliotecaController extends Controller
{
    private $messages = [
        'nome_biblioteca.required' => 'O campo "nome da biblioteca" é necessário para prosseguir.',
        'nome_biblioteca.max' => 'O nome da biblioteca deve ter no máximo 50 caracteres.',
        'usuario.required' => 'O campo usuario é necessário para prosseguir.',
        'usuario.unique' => 'Usuário já existente, escolha outro.',
        'usuario.max' => 'A senha deve ter no máximo 25 caracteres.',
        'senha.required' => 'O campo senha é necessário para prosseguir.',
        'senha.min' => 'A senha deve ter no mínimo 6 caracteres.',
        'senha.max' => 'A senha deve ter no máximo 12 caracteres.'
    ];

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data, string $action = '')
    {
        switch($action){
            case 'insert':
                return Validator::make($data, [
                    'nome_biblioteca' => 'required|string|max:50',
                    'usuario' => 'required|unique:bibliotecas,usuario|max:25',
                    'senha' => 'required|string|min:6|max:12'
                ], $this->messages);
            break;
            case 'update':
                return Validator::make($data, [
                    'nome_biblioteca' => 'string|max:70',
                    'usuario' => 'string|max:100',
                    'senha' => 'string|min:6|max:12'
                ], $this->messages);
            break;
        }
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bibliotecas = Biblioteca::all();

        return response()->json($bibliotecas, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
            $data = $request->all();
            $validation = $this->validator($data, 'insert');
            $errors = $validation->errors();
            
            if(sizeof($errors) > 0)
                return response()->json(['data'=> ['error' => $errors]], 422);
            
            DB::beginTransaction();
            $biblioteca = Biblioteca::create($data);    
            DB::commit();
            
            return response()->json(['data'=> $biblioteca], 200);
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();

            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function show($id_biblioteca)
    {
        $biblioteca = Biblioteca::find($id_biblioteca);
        $status = 200;

        if($biblioteca == null){
            $status = 404;
        }

        return response()->json(
            ['data'=> $biblioteca], 
            $status
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function edit(Biblioteca $biblioteca)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        try{
            $data = $request->all();
            $validation = $this->validator($data, 'update');
            $errors = $validation->errors();

            $biblioteca = Biblioteca::where('usuario', $data['usuario'])->get();

            if(sizeof($biblioteca) > 0){
                $validation->errors()->add('usuario', $this->messages['usuario.required']);
            }
            
            if(sizeof($errors) > 0)
                return response()->json(['data'=> $errors], 422);
            
            $biblioteca = Biblioteca::find($data['id_biblioteca']);    

            DB::beginTransaction();
            $biblioteca->update($data);   
            DB::commit();
            
            return response()->json(['data'=> $biblioteca], 200);
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();
            
            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Biblioteca  $biblioteca
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        try{
            $data = $request->all();
            
            DB::beginTransaction();
            $biblioteca = Biblioteca::find($request['id_biblioteca']);
            $biblioteca->livros()->forceDelete();
            $biblioteca->forceDelete();
            DB::commit();
            
            return response()->json(
                [
                    'data'=> []
                ], 
                200
            );
        }catch(Exception $ex){
            DB::rollback();
            DB::commit();

            if (config('app.debug')) {
                return response()->json(
                    [
                        'data'=> [
                            'errors' => ['server' => $ex->getMessage()]
                        ]
                    ], 
                    400
                );
            }

            return response()->json(
                [
                    'data'=> [
                        'errors' => ['server' =>'Erro ao executar operação!']
                    ]
                ], 
                400
            );
        }
    }
}
