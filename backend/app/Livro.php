<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livro extends Model
{
    protected $primaryKey = 'id_livro';
    public $timestamps = false;
    
    protected $fillable = ['id_livro', 'nome_livro', 'autor', 'dt_publicacao','editora','quantidade','id_biblioteca'];

   
    public function biblioteca()
    {
        return $this->belongsTo('App\Biblioteca', 'id_biblioteca');
    }
}
