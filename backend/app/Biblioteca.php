<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Biblioteca extends Model
{
    protected $primaryKey = 'id_biblioteca';
    public $timestamps = false;
    
    protected $fillable = ['id_biblioteca', 'nome_biblioteca', 'usuario', 'senha'];

   
    public function livros()
    {
        return $this->hasMany('App\Livro', 'id_biblioteca');
    }
}
